CREATE TABLE CELCAT AS SELECT * FROM thierry_millan.CELCAT2017;

CREATE TABLE MAT (
    MATC varchar2(10 BYTE) PRIMARY KEY,
    INTC varchar2(70 BYTE)
);

INSERT INTO MAT(MATC, INTC) SELECT DISTINCT(MATC), INTC FROM CELCAT WHERE MATC is not NULL;


CREATE TABLE GROUPE (
    GRPC varchar2(20 BYTE) PRIMARY KEY,
    ANNEEC char(3 BYTE)
);

INSERT INTO GROUPE(GRPC, ANNEEC) SELECT DISTINCT(GRPC), ANNEEC FROM CELCAT WHERE GRPC is not NULL;


CREATE TABLE CRENEAU (

    DEBSEMC date,
    JOURC varchar2(8 byte),
    HEUREDC char(5 byte),
    GRPC varchar2(20 byte),
    TYPEC varchar2(20 byte),
    HEUREFC char(5 byte),
    MATC varchar2(10 byte),
    
    
    FOREIGN KEY(MATC) REFERENCES MAT(MATC),
    FOREIGN KEY(GRPC) REFERENCES GROUPE(GRPC),
    PRIMARY KEY(DEBSEMC, JOURC, HEUREDC, GRPC)

);

SELECT COUNT(*) FROM CELCAT;

INSERT INTO CRENEAU(DEBSEMC, JOURC, HEUREDC, GRPC, TYPEC, HEUREFC, MATC)
SELECT DISTINCT DEBSEMC, JOURC, HEUREDC, GRPC, TYPEC, HEUREFC, MATC FROM CELCAT WHERE GRPC is not NULL;


CREATE TABLE ENSEIGNER (

    DEBSEMC date,
    JOURC varchar2(8 byte),
    HEUREDC char(5 byte),
    GRPC varchar2(20 byte),
    ENSC char(3 byte),
    
    FOREIGN KEY(DEBSEMC, JOURC, HEUREDC, GRPC) REFERENCES CRENEAU(DEBSEMC, JOURC, HEUREDC, GRPC),
    PRIMARY KEY(DEBSEMC, JOURC, HEUREDC, GRPC, ENSC)

);

INSERT INTO ENSEIGNER(DEBSEMC, JOURC, HEUREDC, GRPC, ENSC)
SELECT DISTINCT DEBSEMC, JOURC, HEUREDC, GRPC, ENSC FROM CELCAT WHERE GRPC is not NULL AND ENSC is not null;

CREATE TABLE AFFECTER (

    DEBSEMC date,
    JOURC varchar2(8 byte),
    HEUREDC char(5 byte),
    GRPC varchar2(20 byte),
    SALC varchar2(15 byte),
    
    FOREIGN KEY(DEBSEMC, JOURC, HEUREDC, GRPC) REFERENCES CRENEAU(DEBSEMC, JOURC, HEUREDC, GRPC),
    PRIMARY KEY(DEBSEMC, JOURC, HEUREDC, GRPC, SALC)

);

INSERT INTO AFFECTER(DEBSEMC, JOURC, HEUREDC, GRPC, SALC)
SELECT DISTINCT DEBSEMC, JOURC, HEUREDC, GRPC, SALC FROM CELCAT WHERE SALC is not null;



/* Requêtes */


SELECT DEBSEMC, JOURC, HEUREDC, TYPEC, HEUREFC, GRPC, MATC, SALC, ENSC, ANNEEC, INTC
FROM CELCAT
MINUS
SELECT CRENEAU.DEBSEMC, CRENEAU.JOURC, CRENEAU.HEUREDC, TYPEC, HEUREFC, CRENEAU.GRPC, CRENEAU.MATC, SALC, ENSC, ANNEEC, INTC
FROM CRENEAU, MAT, GROUPE, ENSEIGNER, AFFECTER
WHERE CRENEAU.GRPC  =   GROUPE.GRPC
AND MAT.MATC        =   CRENEAU.MATC
AND AFFECTER.DEBSEMC=   CRENEAU.DEBSEMC
AND AFFECTER.JOURC  =   CRENEAU.JOURC
AND AFFECTER.HEUREDC=   CRENEAU.HEUREDC
AND AFFECTER.GRPC   =   CRENEAU.GRPC
AND ENSEIGNER.DEBSEMC=  CRENEAU.DEBSEMC
AND ENSEIGNER.JOURC =   CRENEAU.JOURC
AND ENSEIGNER.HEUREDC=  CRENEAU.HEUREDC
AND ENSEIGNER.GRPC  =   CRENEAU.GRPC;


CREATE TABLE FORMATION AS SELECT * FROM THIERRY_MILLAN.formation2018;
CREATE TABLE SALLE AS SELECT * FROM THIERRY_MILLAN.salle2018;
CREATE TABLE ENSEIGNANT AS SELECT * FROM THIERRY_MILLAN.enseignant2018;
CREATE TABLE STATUT AS SELECT * FROM THIERRY_MILLAN.statut2018;


ALTER TABLE FORMATION ADD CONSTRAINT PK_FORMATION PRIMARY KEY (IdFor);
ALTER TABLE SALLE ADD CONSTRAINT PK_SALLE PRIMARY KEY (NSalle);
ALTER TABLE ENSEIGNANT ADD CONSTRAINT PK_ENSEIGNANT PRIMARY KEY (IdEnseign);
ALTER TABLE STATUT ADD CONSTRAINT PK_STATUT PRIMARY KEY (Grade);

ALTER TABLE GROUPE ADD CONSTRAINT FK_GROUPE_ANNEEC FOREIGN KEY (ANNEEC) REFERENCES FORMATION(IdFor);
ALTER TABLE ENSEIGNANT ADD CONSTRAINT FK_ENSEIGNANT_GRADE FOREIGN KEY (Grade) REFERENCES STATUT(Grade);
ALTER TABLE ENSEIGNER ADD CONSTRAINT FK_ENSEIGNER_ENSC FOREIGN KEY (ENSC) REFERENCES ENSEIGNANT(IdEnseign);
ALTER TABLE AFFECTER ADD CONSTRAINT FK_AFFECTER_SALC FOREIGN KEY (SALC) REFERENCES SALLE(NSalle);

ALTER TABLE GROUPE ADD Eff number(3) DEFAULT 0;

UPDATE GROUPE SET Eff=(SELECT Eff FROM THIERRY_MILLAN.efformation2018 WHERE IDGRP = GROUPE.GRPC);

/* Requête SQL (4) */
/* Petit a*/
SELECT GROUPE.GRPC, GROUPE.Eff
FROM GROUPE, CRENEAU
WHERE CRENEAU.MATC = 'InM1101'
AND CRENEAU.DEBSEMC = '19/09/16'
AND CRENEAU.JOURC = 'mercredi'
AND CRENEAU.HEUREDC = '11:00'
AND CRENEAU.grpc = GROUPE.GRPC;

/* Petit b (nul) */
SELECT m.MATC, c.TYPEC, SUM(
    CASE
        WHEN c.TYPEC = 'CM' THEN ((to_date(c.HEUREFC, 'HH24:MI') - to_date(c.HEUREDC, 'HH24:MI'))*24*1.5)
        ELSE ((to_date(c.HEUREFC, 'HH24:MI') - to_date(c.HEUREDC, 'HH24:MI'))*24)
    END
) AS NBHEURE
FROM MAT m, CRENEAU c
WHERE m.matc = c.matc
  AND c.TYPEC in ('TD', 'TP', 'CM')
GROUP BY m.MATC, c.TYPEC
ORDER BY 1, 2;

/* Petit b (bien) */
SELECT m.MATC, sum(  (to_date(c.HEUREFC, 'HH24:MI') - to_date(c.HEUREDC, 'HH24:MI')) * 24 * case typec when 'CM' then 1.5 else 0 end ) CM,
               sum(  (to_date(c.HEUREFC, 'HH24:MI') - to_date(c.HEUREDC, 'HH24:MI')) * 24 * case typec when 'TD' then 1 else 0 end ) TD,
               sum(  (to_date(c.HEUREFC, 'HH24:MI') - to_date(c.HEUREDC, 'HH24:MI')) * 24 * case typec when 'TP' then 1 else 0 end ) TP
FROM MAT m, CRENEAU c
WHERE m.matc = c.matc
  AND c.TYPEC in ('TD', 'TP', 'CM')
GROUP BY m.MATC
ORDER BY 1;


/* Petit c */
CREATE OR REPLACE VIEW NbHdispenseParEnseignant AS
SELECT e.idenseign as IdEnseignant, sum(
    (to_date(c.HEUREFC, 'HH24:MI') - to_date(c.HEUREDC, 'HH24:MI'))*24 * (
    CASE c.TYPEC
        WHEN 'CM' THEN 1.5
        ELSE 1
    END
    )
) AS NbH
FROM ENSEIGNANT e, CRENEAU c, ENSEIGNER ee
WHERE e.idenseign = ee.ensc
AND ee.heuredc = c.heuredc
AND ee.debsemc = c.debsemc
AND ee.jourc = c.jourc
AND c.grpc = ee.grpc
AND c.matc IS NOT NULL
GROUP BY e.idenseign
ORDER BY 1, 2;

SELECT * FROM nbhdispenseparenseignant;

/* 5 : Les Triggers */
CREATE OR REPLACE TRIGGER not_good_room
BEFORE INSERT OR UPDATE
ON AFFECTER
FOR EACH ROW
DECLARE
    v_typeSalle varchar2(5 CHAR);
    v_typeCours varchar2(5 CHAR);
    not_the_good_room EXCEPTION;
    PRAGMA EXCEPTION_INIT (not_the_good_room, -00001);
BEGIN
    SELECT s.tsal INTO v_typeSalle FROM SALLE s WHERE s.NSALLE = :new.salc;
    SELECT c.TYPEC INTO v_typeCours FROM CRENEAU c WHERE c.debsemc = :new.debsemc AND c.jourc = :new.jourc AND c.heuredc = :new.heuredc AND c.grpc = :new.grpc;
    IF v_typeCours = 'TP' THEN
        IF  v_typeSalle <> 'TP' THEN
                RAISE not_the_good_room;
        END IF;
    END IF;
EXCEPTION
    WHEN not_the_good_room THEN
        RAISE_APPLICATION_ERROR(num => -20001, msg => 'La salle ne convient pas à l''enseignement');
    WHEN OTHERS THEN NULL;
END not_good_room;

/* Suppresion du TRIGGER (Inutile sachant qu'on utilise CREATE OR REPLACE)*/
DROP TRIGGER not_good_room;

/* Tests du TRIGGER et des Constraints */
INSERT INTO AFFECTER VALUES('05/06/2017', 'mardi', '08:00', 'InS2F2', 'IN209');
DELETE FROM AFFECTER WHERE debsemc = '05/06/2017' AND jourc = 'mardi' AND heuredc = '08:00' AND grpc = 'InS2F2' AND salc = 'IN209';

INSERT INTO AFFECTER VALUES('05/06/2017', 'mardi', '08:00', 'InS2F2', 'InJade');

rollback;

INSERT INTO AFFECTER VALUES('05/06/2017', 'mardi', '08:00', 'InSxx', 'IN209');

INSERT INTO AFFECTER VALUES('05/06/2017', 'mardi', '08:00', 'InS2F2', 'InXXX');

/* TEST : */

/*  Celui ci ne DEVRAIT pas marcher,
    si ce n'est pas le cas, il faut modifier le trigger
    et ajouter OR UPDATE aprés le AFTER INSERT
    pour que le trigger s'active même quand on execute un UPDATE*/
UPDATE AFFECTER SET SALC = 'IN209'
WHERE AFFECTER.DEBSEMC  = '05/06/17'
AND AFFECTER.JOURC      = 'vendredi'
AND AFFECTER.HEUREDC    = '08:00'
AND AFFECTER.GRPC       = 'InS2A1'
AND AFFECTER.SALC       = 'InRubis';

/* Celui ci doit marcher */
UPDATE AFFECTER SET SALC = 'InJade'
WHERE AFFECTER.DEBSEMC  = '05/06/17'
AND AFFECTER.JOURC      = 'vendredi'
AND AFFECTER.HEUREDC    = '08:00'
AND AFFECTER.GRPC       = 'InS2A1'
AND AFFECTER.SALC       = 'InRubis';

rollback;

/* Trigger n°2*/
CREATE OR REPLACE TRIGGER not_enought_place
BEFORE INSERT OR UPDATE
ON AFFECTER
FOR EACH ROW
DECLARE
    v_effectifSalle number;
    v_effectifGroupe number;
BEGIN
    SELECT s.capacite INTO v_effectifSalle FROM SALLE s WHERE s.NSALLE = :new.salc;
    SELECT g.eff INTO v_effectifGroupe FROM GROUPE g WHERE g.grpc = :new.grpc;
    IF v_effectifSalle < v_effectifGroupe THEN
        RAISE_APPLICATION_ERROR(num => -20002, msg => 'La salle est trop petite');
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN return;
END not_enought_place;
/

/* TEST , ils doivent tous passer*/
INSERT INTO CRENEAU VALUES ('17/07/17', 'mercredi', '17:00', 'CM', '18:30', 'InS2', 'InM2203');
INSERT INTO AFFECTER VALUES ('17/07/17', 'mercredi', '17:00', 'InS2', 'IR028');

INSERT INTO CRENEAU VALUES ('17/07/17', 'mercredi', '17:00', 'CM', '18:30', 'InS2', 'InM2203');
INSERT INTO AFFECTER VALUES ('17/07/17', 'mercredi', '17:00', 'InS2', 'IN203');


INSERT INTO CRENEAU VALUES ('17/07/17', 'mercredi', '17:00', 'CM', '18:30', 'InS2', 'InM2203');
INSERT INTO AFFECTER VALUES ('17/07/17', 'mercredi', '17:00', 'InS2', 'IR028');

ALTER TRIGGER NOT_GOOD_ROOM DISABLE;
DROP TRIGGER NOT_GOOD_ROOM;
ALTER TRIGGER not_enought_place ENABLE;



UPDATE AFFECTER
SET SALC = 'InJade'
WHERE DEBSEMC = '24/04/17'
AND JOURC = 'lundi'
AND HEUREDC = '15:30'
AND GRPC = 'InS2'
AND SALC = 'IR028';

SELECT s.capacite FROM SALLE s WHERE s.nsalle = 'InJade';
SELECT g.eff FROM GROUPE g WHERE g.grpc = 'InS2';

COMMIT;

CREATE OR REPLACE TRIGGER not_enought_place
BEFORE INSERT OR UPDATE
ON AFFECTER
FOR EACH ROW
DECLARE
    v_effectifSalle number;
    v_effectifGroupe number;
BEGIN
    SELECT s.capacite INTO v_effectifSalle FROM SALLE s WHERE s.NSALLE = :new.salc;
    SELECT g.eff INTO v_effectifGroupe FROM GROUPE g WHERE g.grpc = :new.grpc;
    IF v_effectifSalle < v_effectifGroupe THEN
        RAISE_APPLICATION_ERROR(num => -20002, msg => 'La salle est trop petite');
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN return;
END not_enought_place;


CREATE OR REPLACE TRIGGER t_b_i_creneau_chevauche
BEFORE INSERT
ON CRENEAU
FOR EACH ROW
DECLARE
BEGIN
/* to_date(d.HEUREFC, 'HH24:MI') */
/* on verif si heuredc de la nouvelle insertion est supérieure à la selection 
ET
   on verif si heuredc de la nouvelle insertion est inférieure à la selection*/

    if to_date(:new.heuredc, 'HH24:MI') > SELECT to_date(c.heurefc, 'HH24:MI')
                                            FROM CRENEAU c
                                            WHERE c.grpc = :new.grpc
                                            AND c.debsemc = :new.debsemc
                                            AND c.jourc = :new.jourc
                                            AND c.matc = :new.matc
        then
        
    end if;
   
EXCEPTION
    WHEN NO_DATA_FOUND THEN return;
END not_enought_place;




